/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klondike;

import klondike.structures.Smithy;
import klondike.structures.World;

/**
 *
 * @author jaspertomas
 */
public class Klondike {
    public static void main(String[] args)
    {
        ProcessResource.initProcesses();
        
        World world=World.getInstance();
        Smithy smithy=new Smithy();
        world.addChild(smithy);
        
        Inventory.print();
        System.out.println();
        
        smithy.addProcess(ProcessType.MAKEIRON);
        World.getInstance().turn();

        Inventory.print();
    }
}
