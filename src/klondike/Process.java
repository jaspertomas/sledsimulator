/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klondike;

import java.util.ArrayList;

/**
 *
 * @author jaspertomas
 */
public class Process {
    public Process(Integer processType)
    {
        this.processType=processType;
        this.turnsRemaining=ProcessType.durations[processType];
    }
    
    Integer processType=0;
    Integer turnsRemaining=0;
    public Integer getProcessType() {
        return processType;
    }
    public Integer getTurnsRemaining() {
        return turnsRemaining;
    }
    
    
    public void turn(){if(turnsRemaining!=0)turnsRemaining--;}
    public Boolean isComplete(){return turnsRemaining==0;}
}
