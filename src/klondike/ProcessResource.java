/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klondike;

import java.util.HashMap;

/**
 *
 * @author jaspertomas
 */
public class ProcessResource {
    //process requirements
    public static HashMap<Integer,HashMap<Integer,Integer>> processInput=new HashMap<Integer,HashMap<Integer,Integer>>();
    public static HashMap<Integer,HashMap<Integer,Integer>> processOutput=new HashMap<Integer,HashMap<Integer,Integer>>();
    public static void initProcesses()
    {
        HashMap<Integer,Integer> map;
        map=new HashMap();
            map.put(ResourceType.ORE, -1);
            map.put(ResourceType.FIRE, -3);
            map.put(ResourceType.WORKER, -3);
        processInput.put(ProcessType.MAKEIRON,map);
        map=new HashMap();
            map.put(ResourceType.IRON, 1);
            map.put(ResourceType.WORKER, 3);
        processOutput.put(ProcessType.MAKEIRON,map);

        map=new HashMap();
            map.put(ResourceType.IRON, -1);
            map.put(ResourceType.FIRE, -5);
            map.put(ResourceType.WORKER, -3);
        processInput.put(ProcessType.MAKEWIRE,map);
        map=new HashMap();
            map.put(ResourceType.WIRE, 1);
            map.put(ResourceType.WORKER, 3);
        processOutput.put(ProcessType.MAKEWIRE,map);

        map=new HashMap();
            map.put(ResourceType.WIRE, -1);
            map.put(ResourceType.FIRE, -5);
            map.put(ResourceType.WORKER, -3);
        processInput.put(ProcessType.MAKENAIL,map);
        map=new HashMap();
            map.put(ResourceType.NAIL, 1);
            map.put(ResourceType.WORKER, 3);
        processOutput.put(ProcessType.MAKENAIL,map);

        map=new HashMap();
            map.put(ResourceType.CABLE, -1);
            map.put(ResourceType.WIRE, -3);
            map.put(ResourceType.WORKER, -3);
        processInput.put(ProcessType.MAKECHAIN,map);
        map=new HashMap();
            map.put(ResourceType.CHAIN, 1);
            map.put(ResourceType.WORKER, 3);
        processOutput.put(ProcessType.MAKECHAIN,map);

        map=new HashMap();
            map.put(ResourceType.IRON, -3);
            map.put(ResourceType.NAIL, -1);
            map.put(ResourceType.WORKER, -3);
        processInput.put(ProcessType.MAKESAW,map);
        map=new HashMap();
            map.put(ResourceType.SAW, 1);
            map.put(ResourceType.WORKER, 3);
        processOutput.put(ProcessType.MAKESAW,map);

    
    }
        
}
