/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klondike.structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import klondike.Inventory;
import klondike.Process;
import klondike.ProcessResource;

/**
 *
 * @author jaspertomas
 */
public abstract class Structure {
    protected ArrayList<Structure> children=new ArrayList<Structure>();
    protected ArrayList<Structure> getChildren(){return children;}
    public Boolean addChild(Structure child){children.add(child);return true;}
    public Boolean turn() {
        Boolean success=true;
        
        for(Structure c:children)
        {
            c.turn();
        }
        
        for(Process p:getProcesses())
        {
            HashMap<Integer,Integer> processoutputs=ProcessResource.processOutput.get(p.getProcessType());
            for(Integer resourcetype:processoutputs.keySet())
            {
                Integer qty=processoutputs.get(resourcetype);
                Inventory.inventory[resourcetype]+=qty;
            }
        }
        
        return success;
    }
    
    

    protected Integer[] processesAllowed={
        };

    
    abstract public Integer[] getProcessesAllowed();
    
    protected ArrayList<Process> processes=new ArrayList<Process> ();
    public ArrayList<Process> getProcesses() {
        return processes;
    }
    
    public Boolean addProcess(Integer processtype)
    {
        if(!Arrays.asList(getProcessesAllowed()).contains(processtype))
            return false;
        
        processes.add(new Process(processtype) {});
        //deduct inputs
        HashMap<Integer,Integer> processinputs=ProcessResource.processInput.get(processtype);
        for(Integer resourcetype:processinputs.keySet())
        {
            Integer qty=processinputs.get(resourcetype);
            Inventory.inventory[resourcetype]+=qty;
        }
        
        return true;
    }
}
