/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klondike.structures;

import klondike.ProcessType;

/**
 *
 * @author jaspertomas
 */
public class Smithy extends Structure {

    Integer[] processesAllowed={
        ProcessType.MAKENAIL,
        ProcessType.MAKESAW,
        ProcessType.MAKEIRON,
        ProcessType.MAKECHAIN,
        ProcessType.MAKEWIRE,
        };
    @Override public Integer[] getProcessesAllowed() {
        return processesAllowed;
    }
    
    /*
    in a database
    there will be tables:
    resourcetype: name
    inventory: reourcetype_id, qty
    processtype: name, duration

    processinput: processtype_id, itemtype_id, qty
    processoutput: processtype_id, itemtype_id, qty
    or
    processresource: processtype_id, itemtype_id, qty, type (in or out)
    
    //these are the actual running processes
    process: process_type_id, structure_id, turnsremaining  //potential fields: qty=1, status=pending/complete
    //turnsremaining counts down from producttype.duration to 0. 0 turns remaining means complete. No need for status anymore
    
    //warehouse? not for now. This will require an entire new level of complexity including delivery receipts and reports
    
    */
}
