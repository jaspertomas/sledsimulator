/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klondike;

/**
 *
 * @author jaspertomas
 */
public class Inventory {

    //inventory
    public static Integer[] inventory={0,0,0,0,0,0,0,0,0,0};

    public static void print()
    {
        System.out.println("ores: "+inventory[ResourceType.ORE]);
        System.out.println("Irons: "+inventory[ResourceType.IRON]);
        System.out.println("wires: "+inventory[ResourceType.WIRE]);
        System.out.println("nails: "+inventory[ResourceType.NAIL]);
        System.out.println("chains: "+inventory[ResourceType.CHAIN]);
        System.out.println("saws: "+inventory[ResourceType.SAW]);
        System.out.println("cables: "+inventory[ResourceType.CABLE]);
        System.out.println("fire: "+inventory[ResourceType.FIRE]);
        System.out.println("workers: "+inventory[ResourceType.WORKER]);
    }
}
