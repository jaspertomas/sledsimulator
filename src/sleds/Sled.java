/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sleds;

import java.util.ArrayList;

/**
 *
 * @author jaspertomas
 */
public class Sled {
    
    public static void main(String args[])
    {
        Sled.initSleds();
        
        for(Sled sled:sleds)
        {
            System.out.println(sled.name);
            sled.runmulti();
        }
        
        
        
    }
    
    public static final Integer PORRIGE=1;
    public static final Integer FISH=2;
    
    public static final Integer STATION=0;
    public static final Integer WINDSONG=1;
    public static final Integer AERY=2;
    public static final Integer POLARSIDE=3;
    
    
    String name;
    Integer range=0;
    Integer fishperday=0;
    Integer porrigeperday=0;
    Integer capacity=0;
    Integer dogs=0;
    Integer food=0;
    Integer kmperday=0;
    
    Integer distance=0;
    Integer daystraveled=0;
    Integer fishconsumed=0;
    Integer porrigeconsumed=0;

    Integer totaldistance=0;
    Integer totaldaystraveled=0;
    Integer totalfishconsumed=0;
    Integer totalporrigeconsumed=0;
    
    Boolean runFailed=false;

    public static ArrayList<Sled> sleds=new ArrayList<Sled>();
    public static void initSleds()
    {
                                //porrige, fish, capacity, range
        sleds.add(new Sled("Small Sled",1,0,100,100));
        sleds.add(new Sled("Light Wind",2,0,120,125));
        sleds.add(new Sled("Large Sled",2,0,250,100));
        sleds.add(new Sled("Eagle Sled",4,0,300,125));
        sleds.add(new Sled("Shaman Sled",2,2,400,150));
        sleds.add(new Sled("Freight Sled",0,4,600,150));
        sleds.add(new Sled("Indigo Sled",0,4,800,180));
        sleds.add(new Sled("Modern Sled",0,6,1000,210));
        sleds.add(new Sled("Horse Sled",1,1,900,180));
        
    }

    public Sled(    
        String name,
        Integer porrigeperday,
        Integer fishperday,
        Integer capacity,
        Integer range
            ) {
        this.name=name;
        this.fishperday=fishperday;
        this.porrigeperday=porrigeperday;
        this.capacity=capacity;
        this.range=range;
        this.kmperday=range/6;
    }
    
    
    
    Integer[][] distances={
        {0,89,190,246},
        {89,0,124,165},
        {190,124,0,69},
        {246,165,69,0},
        };
    Integer paths[][][]=
    {
        {{0,3}},
        {{0,1},{1,3}},
        {{0,2},{2,3}},
        {{0,1},{1,2},{2,3}},
    };
    
    public Integer[] runmulti()
    {
        Integer[] totaloutput={0,0,0,0};
        Integer[] output={0,0,0,0};
        
        for(Integer[][] path:paths)
        {
            for(Integer[] subpath:path)
            {
                run(subpath[0],subpath[1]);
            }
            printOutput();
            resetTotals();
        }
        
        
        return output;
    }
    public void run(Integer source, Integer destination)
    {
        if(runFailed)return;
        
        //add modifiers here
        Integer newkmperday=kmperday+15-4-4;
        
        distance = distances[source][destination];
        totaldistance+=distance;
        
        daystraveled=Double.valueOf(Math.ceil(distance/Double.valueOf(newkmperday))).intValue();
        if(daystraveled>6){runFailed=true;return;}
        totaldaystraveled+=daystraveled;
        
        fishconsumed=daystraveled*fishperday;
        totalfishconsumed+=fishconsumed;

        porrigeconsumed=daystraveled*porrigeperday;
        totalporrigeconsumed+=porrigeconsumed;
    }
    public void resetTotals()
    {
        runFailed=false;
        totaldistance=0;
        totalporrigeconsumed=0;
        totalfishconsumed=0;
        totaldaystraveled=0;
    }
    public Integer getPackages()
    {
        Integer newcapacity=capacity;
        return Double.valueOf(Math.floor(newcapacity/21d)).intValue();
    }

    public Integer getTotaldistance() {
        return totaldistance;
    }

    public Integer getTotaldaystraveled() {
        return totaldaystraveled;
    }

    public Integer getTotalfishconsumed() {
        return totalfishconsumed;
    }

    public Integer getTotalporrigeconsumed() {
        return totalporrigeconsumed;
    }

    
    
    public void printOutput()
    {
        if(runFailed)
        {
            System.out.println("Failed");
            return;
        }
        
        
        Integer packages=getPackages();
        System.out.println(
            totalporrigeconsumed+" porrige, "
                +totalfishconsumed+" fish, "
                +packages+" packages, "
                +kmperday+" kmperday, "
                +Double.valueOf(Double.valueOf(packages)/totalporrigeconsumed).toString()+" packages per porrige, "
                +Double.valueOf(Double.valueOf(packages)/totalfishconsumed).toString()+" packages per fish, "
                    );
    }
    
}
