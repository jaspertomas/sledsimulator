This is a simulator of Klondike's sled system

It simulates running from Station to Polar Side, calculating how much fuel (porridge and fish) was consumed, how many packages were delivered, and how efficient the fuel:package ratio was.

There are 4 paths and 9 sleds.

The simulator runs each sled on every path and prints the fuel efficiency of the run on the terminal.

"Failed" means that the sled didn't have the ability to make the run at all.

This simulator is my first attempt to create a system of economic simulators.